# What is the base image to pull
FROM ubuntu

# Update package lists
RUN apt-get update && apt-get install -y wget && \
wget https://packages.microsoft.com/config/ubuntu/21.04/packages-microsoft-prod.deb -O packages-microsoft-prod.deb && \
dpkg -i packages-microsoft-prod.deb && \
rm packages-microsoft-prod.deb && \
apt-get update && apt-get install -y apt-transport-https && \
apt-get update && apt-get install -y dotnet-sdk-5.0

RUN apt-get install -y gnupg ca-certificates && \
apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 3FA7E0328081BFF6A14DA29AA6A19B38D3D831EF && \
echo "deb https://download.mono-project.com/repo/ubuntu stable-focal main" | tee /etc/apt/sources.list.d/mono-official-stable.list && \
apt-get install -y mono-complete




RUN apt-get install -y smbclient curl && apt-get update && apt upgrade -y 

# Copy the executable, _Data directory, and .so file to /tmp
COPY b.x86_64 /unity_player/linux_build.x86_64

COPY b_Data /unity_player/linux_build_Data
COPY UnityPlayer.so /unity_player/UnityPlayer.so
#COPY UnityPlayer.so /unity_player2/UnityPlayer.so
COPY WFC /unity_player/WFC

COPY run_simulation.sh /run_simulation.sh
COPY config.txt /temp/exp01/config.json


ENTRYPOINT ["/bin/bash", "run_simulation.sh"]
