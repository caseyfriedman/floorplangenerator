#!/bin/bash


cd /unity_player;

container=$(basename "$(cat /proc/1/cpuset)")
staging="staging"

while true
do
	experiment=`curl -s 'http://10.1.2.24:5000/currentExperiment' | python3 -c "import sys, json; print(json.load(sys.stdin)['experimentName'])"`

        #clean up old layout files
        find /tmp/Simulation/ -maxdepth 3 -type f -name "*.png" -exec rm -rf {} +
        /unity_player/linux_build.x86_64;
		
		dir="/tmp/Simulation/${experiment}/Generated/*"
		threshold=50000000
		for file in ${dir}.wav
		do
			size="$(wc -c <"$file")"
			if [[ "$size" -lt "$threshold" ]]
			then
					filename="${file%.*}"
					csv="${filename}.csv";
					rm $file;
					rm $csv;
			fi
		done
        
		smbclient //filestore.soi.local/M2-1 -c \
		"recurse ON; prompt OFF; mkdir $staging; cd $staging; \
		mkdir $experiment; cd $experiment; mkdir $HOST_HOSTNAME; cd $HOST_HOSTNAME; \
		mkdir $HOSTNAME; cd $HOSTNAME; lcd /tmp/Simulation; lcd $experiment; mput *;"
		rm -r /tmp/Simulation/;
		
        sleep 2;
done
