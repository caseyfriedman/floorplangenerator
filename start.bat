@ECHO OFF

if [%1]==[] goto usage



for /f "usebackq tokens=*" %%a in (`hostname`) do set hostname=%%a

    setlocal enableextensions disabledelayedexpansion

    set "search=PLACEHOLDER"
    set "replace=%hostname%"

    set "textFile=docker-compose.yaml"

    for /f "delims=" %%i in ('type "%textFile%" ^& break ^> "%textFile%" ') do (
        set "line=%%i"
        setlocal enabledelayedexpansion
        >>"%textFile%" echo(!line:%search%=%replace%!
        endlocal
    )


set /A number=%1

docker compose pull
FOR /l %%x IN (1, 1, %number%) DO (
	docker compose up -d --scale simulation=%%x
	timeout /t 2 /NOBREAK > nul
	)

:while
docker compose pull > pull_result
find /c "complete" pull_result >NUL
if %errorlevel% equ 1 goto done
	docker compose down
	FOR /l %%x IN (1, 1, %number%) DO (
	docker compose up -d --scale simulation=%%x
	timeout /t 2 /NOBREAK > nul
	)

:done
timeout /t 60 /NOBREAK > nul
goto :while

:usage
@echo Usage: %0 ^<integer^>
@echo,
@echo Description:
@echo      This command creates the provided number of docker containers.
exit /B 1
